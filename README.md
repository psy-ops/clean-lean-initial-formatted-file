# Clean Lean Initial Formatted File

This is a CLIFF for the ROCK.

The main script is `/scripts/clean-lean-initial-formatted-file.Rmd`.

The rendered version of that file is hosted by GitLab Pages (see the CI/CD configuration in the `.gitlab-ci.yml` file) at https://psy-ops.gitlab.io/clean-lean-initial-formatted-file.
